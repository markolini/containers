package containers;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import domain.Bar;
import domain.Baz;
import domain.Container;
import domain.Foo;
import domain.FooLike;
import domain.ListContainer;
import domain.SetContainer;
import logic.SecretFooSortingAlgorithm;

class ContainerTest {

	private static SecretFooSortingAlgorithm algorithm;

	@BeforeAll
	public static void initialize() {
		FooLike origin = new FooLike() {
			@Override
			public double x() {
				return 0;
			}

			@Override
			public double y() {
				return 0;
			}
		};

		algorithm = new SecretFooSortingAlgorithm(origin);
	}

	@Test
	public void listDuplicates() {
		final Foo[] fooArray = new Foo[] { new Foo("5", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("4", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("3", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("2", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("1", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("0", new Baz(2, "x = 2"), new Bar(2, "y = 2")) };

		final ListContainer<FooLike> foos = new ListContainer<FooLike>(Arrays.asList(fooArray));
		final Container<? extends FooLike> sortedFoos = algorithm.doSomething(foos);

		assertTrue(sortedFoos.size() == 1);
	}

	@Test
	public void seDuplicates() {
		final Foo[] fooArray = new Foo[] { new Foo("5", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("4", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("3", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("2", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("1", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("0", new Baz(2, "x = 2"), new Bar(2, "y = 2")) };

		final SetContainer<FooLike> foos = new SetContainer<FooLike>(Arrays.asList(fooArray));
		final Container<? extends FooLike> sortedFoos = algorithm.doSomething(foos);

		assertTrue(sortedFoos.size() == 1);
	}

	@Test
	public void emptyListSorter() {
		final Foo[] fooArray = new Foo[] {};
		final ListContainer<FooLike> foos = new ListContainer<FooLike>(Arrays.asList(fooArray));
		final Container<? extends FooLike> sortedFoos = algorithm.doSomething(foos);

		assertTrue(sortedFoos.size() == 0);
	}

	@Test
	void emptySetSorter() {
		final Foo[] fooArray = new Foo[] {};
		final SetContainer<FooLike> foos = new SetContainer<FooLike>(Arrays.asList(fooArray));
		final Container<? extends FooLike> sortedFoos = algorithm.doSomething(foos);

		assertTrue(sortedFoos.size() == 0);
	}
}
