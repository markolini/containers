package logic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

import domain.Container;
import domain.FooLike;

public class SecretFooSortingAlgorithm implements GenericAlgorithm<FooLike> {
	private final FooLike origin;

	public SecretFooSortingAlgorithm(FooLike origin) {
		this.origin = origin;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Container<? extends FooLike> doSomething(Container<FooLike> argument) {
		final List<FooLike> unsafeList = new ArrayList<>();
		argument.forEach(unsafeList::add);
		argument.forEach(unsafeList::add);

		unsafeList.sort(Comparator.comparing(euclideanDistance(origin)));

		Container<FooLike> resultingContainer;

		try {
			// Using java reflection to create the same type as the input argument
			resultingContainer = (Container<FooLike>) Class.forName(argument.getClass().getName()).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			throw new IllegalStateException("Problem during container instatiation. Cause: " + e.getLocalizedMessage());
		}

		unsafeList.forEach(resultingContainer);

		return resultingContainer;
	}

	private Function<FooLike, Double> euclideanDistance(FooLike origin) {
		return point -> Math.sqrt(Math.pow(point.x() - origin.x(), 2) + Math.pow(point.y() - origin.y(), 2));
	}
}
