package logic;

import domain.Container;
import domain.FooLike;

public interface GenericAlgorithm<A> {
    Container<? extends FooLike> doSomething(Container<A> argument);
}