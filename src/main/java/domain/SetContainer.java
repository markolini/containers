package domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

public class SetContainer<C> extends CollectionContainer<C> {
	public SetContainer() {
		super(new HashSet<C>());
	}

	public SetContainer(Collection<C> collection) {
		super(collection.stream().collect(Collectors.toCollection(HashSet::new)));
	}

	@Override
	public void accept(C element) {
		collection.add(element);
	}
}
