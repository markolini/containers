package domain;

import java.util.Objects;

public class Bar {
	private final double y;
	private final String label;

	public Bar(double y, String label) {
		this.y = y;
		this.label = label;
	}

	@Override
	public String toString() {
		return "Bar{" + "y=" + this.y + ", label='" + this.label + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Bar bar = (Bar) o;
		return Double.compare(bar.getY(), this.y) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.y);
	}

	public double getY() {
		return this.y;
	}

	public String getLabel() {
		return label;
	}
}
