package domain;

import java.util.Collection;
import java.util.Iterator;

/**
 * Middle layer class that provides some of the common implementations for the
 * container classes. Purpose of this class is to reduce duplicated code.
 */
public abstract class CollectionContainer<C> implements Container<C> {
	
	protected Collection<C> collection;
	
	public CollectionContainer(Collection<C> collection) {
		this.collection = collection;
	}

	@Override
	public Iterator<C> iterator() {
		return collection.iterator();
	}
	
	@Override
	public int size() {	
		return collection.size();
	}
}
