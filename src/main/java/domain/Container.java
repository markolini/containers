package domain;

import java.util.function.Consumer;

public interface Container<C> extends Iterable<C>, Consumer<C> {
	int size();
}
