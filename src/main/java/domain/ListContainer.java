package domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class ListContainer<C> extends CollectionContainer<C> {
	public ListContainer() {
		super(new ArrayList<C>());
	}

	public ListContainer(Collection<C> collection) {
		super(collection.stream().collect(Collectors.toCollection(ArrayList::new)));
	}

	@Override
	public void accept(C element) {
		if (collection.contains(element)) {
			return;
		}

		collection.add(element);
	}
}
