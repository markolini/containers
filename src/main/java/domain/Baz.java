package domain;

import java.util.Objects;

public class Baz {
	private final double x;
	private final String label;

	public Baz(double x, String label) {
		this.x = x;
		this.label = label;
	}

	@Override
	public String toString() {
		return "Baz{" + "x=" + this.x + ", label='" + this.label + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Baz baz = (Baz) o;
		return Double.compare(baz.getX(), this.x) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.x);
	}

	public double getX() {
		return this.x;
	}

	public String getLabel() {
		return label;
	}
}
