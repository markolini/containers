package domain;

import java.util.Objects;

public final class Foo implements FooLike {
	private final String id;
	private final Baz baz;
	private final Bar bar;

	public Foo(String id, Baz baz, Bar bar) {
		this.id = id;
		this.baz = baz;
		this.bar = bar;
	}

	@Override
	public double x() {
		return this.baz.getX();
	}

	@Override
	public double y() {
		return this.bar.getY();
	}

	@Override
	public String toString() {
		return "Foo{" + "id='" + this.id + '\'' + ", baz=" + this.baz + ", bar=" + this.bar + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Foo foo = (Foo) o;
		return Objects.equals(this.baz, foo.getBaz()) && Objects.equals(this.bar, foo.getBar());
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.baz, this.bar);
	}

	public String getId() {
		return id;
	}

	public Baz getBaz() {
		return baz;
	}

	public Bar getBar() {
		return bar;
	}
}
