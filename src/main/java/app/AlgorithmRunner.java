package app;

import java.util.Arrays;

import domain.Bar;
import domain.Baz;
import domain.Container;
import domain.Foo;
import domain.FooLike;
import domain.ListContainer;
import domain.SetContainer;
import logic.GenericAlgorithm;
import logic.SecretFooSortingAlgorithm;

public class AlgorithmRunner {
	private static final FooLike origin = new FooLike() {
		@Override
		public double x() {
			return 0;
		}

		@Override
		public double y() {
			return 0;
		}
	};

	public static void main(String[] args) {
		final SecretFooSortingAlgorithm algorithm = new SecretFooSortingAlgorithm(origin);

		final Foo[] foos = new Foo[] { new Foo("5", new Baz(2, "x = 2"), new Bar(2, "y = 2")),
				new Foo("4", new Baz(1, "x = 1"), new Bar(2, "y = 2")),
				new Foo("3", new Baz(1, "x = 1"), new Bar(1, "y = 1")),
				new Foo("2", new Baz(0, "x = 0"), new Bar(1, "y = 1")),
				new Foo("1", new Baz(1, "x = 1"), new Bar(0, "y = 0")),
				new Foo("0", new Baz(0, "x = 0"), new Bar(0, "y = 0")) };

		demonstrateSortingLists(algorithm, foos);
		demonstrateSortingSet(algorithm, foos);
	}

	private static void demonstrateSortingLists(GenericAlgorithm<FooLike> algorithm, Foo[] fooArray) {
		final ListContainer<FooLike> foos = new ListContainer<FooLike>(Arrays.asList(fooArray));
		final Container<? extends FooLike> sortedFoos = algorithm.doSomething(foos);

		printFoos(sortedFoos);
	}

	private static void demonstrateSortingSet(GenericAlgorithm<FooLike> algorithm, Foo[] fooArray) {
		final SetContainer<FooLike> foos = new SetContainer<FooLike>(Arrays.asList(fooArray));
		final Container<? extends FooLike> sortedFoos = algorithm.doSomething(foos);

		printFoos(sortedFoos);
	}

	private static void printFoos(Container<? extends FooLike> sortedFoos) {
		System.out.println("Printing elements from the container: ");

		for (FooLike foolike : sortedFoos) {
			Foo foo = (Foo) foolike;
			System.out.println(foo);
		}

		System.out.println();
	}
}
